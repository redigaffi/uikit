import { Colors } from "./types";

export const baseColors = {
  failure: "#FFFFFF",
  primary: "#FFFFFF",
  primaryBright: "#FFFFFF",
  primaryDark: "#FFFFFF",
  secondary: "#FFFFFF",
  success: "#FFFFFF",
  warning: "#FFFFFF",
};

export const brandColors = {
  binance: "#F0B90B",
};

export const lightColors: Colors = {
  ...baseColors,
  ...brandColors,
  background: "#FFFFFF",
  backgroundDisabled: "#FFFFFF",
  backgroundAlt: "#FFFFFF",
  contrast: "#FFFFFF",
  dropdown: "#FFFFFF",
  invertedContrast: "#FFFFFF",
  input: "#FFFFFF",
  inputSecondary: "#FFFFFF",
  tertiary: "#FFFFFF",
  text: "#FFFFFF",
  textDisabled: "#FFFFFF",
  textSubtle: "#FFFFFF",
  borderColor: "#FFFFFF",
  gradients: {
    bubblegum: "linear-gradient(139.73deg, #E6FDFF 0%, #F3EFFF 100%)",
    cardHeader: "linear-gradient(111.68deg, #F2ECF2 0%, #E8F2F6 100%)",
    blue: "linear-gradient(180deg, #A7E8F1 0%, #94E1F2 100%)",
    violet: "linear-gradient(180deg, #E2C9FB 0%, #CDB8FA 100%)",
  },
};

export const darkColors: Colors = {
  ...baseColors,
  ...brandColors,
  secondary: "#FFFFFF",
  background: "#FFFFFF",
  backgroundDisabled: "#FFFFFF",
  backgroundAlt: "#FFFFFF",
  contrast: "#FFFFFF",
  dropdown: "#FFFFFF",
  invertedContrast: "#FFFFFF",
  input: "#FFFFFF",
  inputSecondary: "#FFFFFF",
  primaryDark: "#FFFFFF",
  tertiary: "#FFFFFF",
  text: "#FFFFFF",
  textDisabled: "#FFFFFF",
  textSubtle: "#FFFFFF",
  borderColor: "#FFFFFF",
  gradients: {
    bubblegum: "linear-gradient(139.73deg, #313D5C 0%, #3D2A54 100%)",
    cardHeader: "linear-gradient(166.77deg, #3B4155 0%, #3A3045 100%)",
    blue: "linear-gradient(180deg, #00707F 0%, #19778C 100%)",
    violet: "linear-gradient(180deg, #6C4999 0%, #6D4DB2 100%)",
  },
};
